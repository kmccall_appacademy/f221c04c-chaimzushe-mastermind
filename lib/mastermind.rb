

class Code
  attr_reader :pegs
  PEGS = {"B" => "blue",
          "G" =>"green",
          "R" => "red",
          "Y" => "yellow",
          "O" => "orange",
          "P" => "purple" }

  # def self.parse(colors)
  #   colors_array = colors.upcase.chars
  #   raise "invalid input" if colors_array.any?{|c| PEGS[c].nil?}
  #   Code.new(colors_array)
  # end

  def self.parse(str)
    # factory method
    pegs = str.split("").map do |letter|
      raise "parse error" unless PEGS.has_key?(letter.upcase)
      PEGS[letter.upcase]
    end

    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times { pegs << PEGS.values.sample }
    Code.new(pegs)
  end

  def initialize(code)
   @pegs = code
  end

  def [](index)
    pegs[index]
  end

  def ==(code)
    return false unless code.is_a?(Code)
     pegs == code.pegs
  end

  def exact_matches(code)
     pegs.select.with_index { |c, i| c == code[i]}.size
  end

  def near_matches(guessed_code)
    guessed_code_counts = guessed_code.color_counts
    nears = 0
    color_counts.each do |color ,count|
      next unless guessed_code_counts.has_key?(color)
      nears += [count, guessed_code_counts[color]].min
    end
    nears - exact_matches(guessed_code)
  end

  def color_counts
    color_count = Hash.new(0)
    pegs.each{|c| color_count[c] += 1}
    color_count
  end

end

class Game
  MAX_GUESSES = 10
  attr_reader :secret_code

  def initialize(code = Code.random)
     @secret_code = code
  end

  def play
    MAX_GUESSES.times do |time|
      guess = get_guess
      if guess == secret_code
        puts "Wow you got it!"
        return
      end
      display_matches(guess)
      puts "You'r at time: #{time +1}. only 10 chances"
    end
    puts "Maybe next time dear"

  end


  def get_guess
    puts "Guess the code:"
    begin
      Code.parse(gets.chomp)
    rescue
      puts "Error parsing code!"
      retry
    end
  end


  def display_matches(code)
    exact = secret_code.exact_matches(code)
    near = secret_code.near_matches(code)
    puts "you have #{exact} exact matches, and #{near} near matches"
  end


end
